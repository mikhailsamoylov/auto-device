<?php

namespace app\components;

/**
 * Class Image
 * @package app\components
 */
class Image
{
    /**
     * @param string $filename
     * @param integer $size
     * @return resource
     */
    public static function resizeImageByLongSide($filename, $size)
    {
        list($width, $height) = getimagesize($filename);
        $longSide = $width >= $height ? 'width' : 'height';
        $ratio = $size / max($width, $height);
        switch ($longSide) {
            case 'width':
                $newWidth = $size;
                $newHeight = $height * $ratio;
                break;
            case 'height':
                $newHeight = $size;
                $newWidth = $width * $ratio;
        }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

        return $newImage;
    }
}