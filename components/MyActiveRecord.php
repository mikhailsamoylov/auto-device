<?php

namespace app\components;

use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * Class MyActiveRecord
 * @package app\components
 */
class MyActiveRecord extends ActiveRecord
{
    /**
     * @param bool $runValidation
     * @param null $attributeNames
     * @return bool|void
     * @throws Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!parent::save($runValidation, $attributeNames)) {
            throw new ValidationException(array_shift($this->errors)[0]);
        }
    }
}