<?php

namespace app\components;

use yii\db\Exception;

/**
 * Class ValidationException
 * @package app\components
 */
class ValidationException extends Exception
{

}