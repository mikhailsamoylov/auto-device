<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=auto_device',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
