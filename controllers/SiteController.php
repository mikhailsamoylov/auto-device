<?php

namespace app\controllers;

use app\components\Image;
use app\components\ValidationException;
use app\models\Album;
use app\models\Photo;
use app\models\Photographer;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\web\Controller;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $albumAndPhotos = (new Query())
            ->select('a.id, p.path_small, a.title, p.date_create')
            ->from('albums a')
            ->leftJoin('photos p', 'p.album_id = a.id')
            ->orderBy('p.date_create DESC')->all();
        $albums = [];
        foreach ($albumAndPhotos as $photo) {
            if (!array_key_exists($photo['id'], $albums)) {
                $albums[$photo['id']] = [
                    'path' => $photo['path_small'],
                    'title' => $photo['title'],
                    'date' => $photo['date_create'],
                    'count' => 0
                ];
            }
            if (!is_null($photo['path_small'])) {
                $albums[$photo['id']]['count']++;
            }
        }
        return $this->render('index', ['albums' => $albums]);
    }

    public function actionAddalbum()
    {
        return $this->render('manipulate_album');
    }

    public function actionSavealbum()
    {
        $title = Yii::$app->request->post('title');
        $description = Yii::$app->request->post('description');
        $name = Yii::$app->request->post('name');
        $email = Yii::$app->request->post('email');
        $phone = Yii::$app->request->post('phone');
        $albumId = Yii::$app->request->post('album_id');

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $author = Photographer::find()->where('name = :name', [':name' => $name])->one();
            if (!$author) {
                $author = new Photographer();
                $author->name = $name;
            }
            if ($email) {
                $author->email = $email;
            }
            if ($phone) {
                $author->phone = $phone;
            }
            $author->save();

            $album = $albumId ? Album::findOne($albumId) : new Album();
            $album->title = $title;
            $album->description = $description;
            $album->photographer_id = $author->id;
            $album->save();
            $transaction->commit();
            $this->redirect('/web/');
        } catch (Exception $e) {
            $transaction->rollBack();
            return $this->render('error', ['message' => $e->getMessage()]);
        }
    }

    public function actionDeletealbum()
    {
        $albumId = (int)substr(Yii::$app->request->get('album_id'), 6);
        Album::deleteAll('id = :id', [':id' => $albumId]);
    }

    public function actionEditalbum()
    {
        $albumId = (int)substr(Yii::$app->request->get('album_id'), 6);
        $albumInfo = (new Query)
            ->select('a.title, a.description, p.name, p.phone, p.email')
            ->from('albums a')
            ->innerJoin('photographers p', 'p.id = a.photographer_id')
            ->where('a.id = :id', [':id' => $albumId])->one();
        return $this->render('manipulate_album', ['albumInfo' => $albumInfo, 'albumId' => $albumId]);
    }

    public function actionAddphoto()
    {
        $albumId = (int)substr(Yii::$app->request->get('album_id'), 6);
        $albums = (new Query())
            ->select('id, title')
            ->from('albums');
        if ($albumId) {
            $albums->where('id = :id', [':id' => $albumId]);
        }
        return $this->render('add_photo', ['albums' => $albums->all()]);
    }

    public function actionSavephoto()
    {
        $title = Yii::$app->request->post('title');
        $address = Yii::$app->request->post('address');
        $albumId = Yii::$app->request->post('album_id');
        $filename = tempnam(Yii::getAlias('@app') . '/resources/photos', 'img');
        unlink($filename);
        $extension = substr($_FILES['file']['name'], -3);
        $filename = substr_replace($filename, $extension, strlen($filename) - 3);
        move_uploaded_file($_FILES['file']['tmp_name'], $filename);

        $filenameSmall = substr_replace($filename, '_small.' . $extension, strlen($filename) - 4);
        imagejpeg(Image::resizeImageByLongSide($filename, 250), $filenameSmall);

        $photo = new Photo();
        $photo->title = $title;
        $photo->address = $address;
        $photo->path = basename($filename);
        $photo->path_small = basename($filenameSmall);
        $photo->album_id = $albumId;
        $photo->save();
        $this->redirect('/web/?r=site/viewalbum&album_id=album_' . $albumId);
    }

    public function actionViewalbum()
    {
        $albumId = (int)substr(Yii::$app->request->get('album_id'), 6);
        $photos = (new Query())
            ->select('id, path, path_small')
            ->from('photos')
            ->where('album_id = :id', [':id' => $albumId])->all();
        return $this->render('view_album', ['photos' => $photos, 'album_id' => $albumId]);
    }

    public function actionDeletephoto()
    {
        $photoId = (int)substr(Yii::$app->request->get('photo_id'), 6);
        Photo::deleteAll('id = :id', [':id' => $photoId]);
    }
}
