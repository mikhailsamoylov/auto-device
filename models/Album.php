<?php

namespace app\models;
use app\components\MyActiveRecord;

/**
 * Class Album
 * @package app\models
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $photographer_id
 * @property string $date_create
 * @property string $date_change
 */
class Album extends MyActiveRecord
{
    public static function tableName()
    {
        return 'albums';
    }

    public function rules()
    {
        return [
            [['title', 'description', 'photographer_id'], 'required'],
            ['title', 'string', 'max' => 50],
            ['description', 'string', 'max' => 200]
        ];
    }
}