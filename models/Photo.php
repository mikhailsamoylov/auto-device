<?php

namespace app\models;
use app\components\MyActiveRecord;

/**
 * Class Photo
 * @package app\models
 *
 * @property integer $id
 * @property string $title
 * @property string $address
 * @property string $path
 * @property string $path_small
 * @property string $date_create
 * @property integer $album_id
 */
class Photo extends MyActiveRecord
{
    public static function tableName()
    {
        return 'photos';
    }

    public function rules()
    {
        return [
            [['title', 'path', 'path_small', 'album_id'], 'required'],
            [['path', 'path_small'], 'unique'],
            ['title', 'string', 'max' => 50],
            ['address', 'string', 'max' => 200],
            ['path', 'string', 'max' => 255]
        ];
    }
}