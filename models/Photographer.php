<?php

namespace app\models;
use app\components\MyActiveRecord;

/**
 * Class Photographer
 * @package app\models
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 */
class Photographer extends MyActiveRecord
{
    public static function tableName()
    {
        return 'photographers';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            [['name', 'email', 'phone'], 'unique'],
            ['name', 'string', 'max' => 50],
            ['email', 'string', 'max' => 255],
            ['phone', 'string', 'length' => 18],
        ];
    }
}