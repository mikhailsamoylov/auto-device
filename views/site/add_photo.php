<?php

/* @var $this yii\web\View
 * @var $albums array
 */

$this->title = 'Добавление фотографии';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <form id="photo" action="/web/?r=site/savephoto" method="post" enctype="multipart/form-data">
        <label>Название фотографии<br/>
            <input type="text" id="photo_title" name="title">
        </label><br/>
        <label>Адрес фотосъёмки<br/>
            <textarea id="address" name="address"></textarea>
        </label><br/>
        <label>Альбом<br/>
            <select id="album" name="album_id">
                <?php
                foreach ($albums as $album) { ?>
                    <option value="<?= $album['id'] ?>"><?= $album['title'] ?></option>
                <?php } ?>
            </select>
        </label><br/>
        <label>Загружаемый файл (не больше 20 Мб)<br/>
            <input type="file" id="file" name="file" accept="image/jpeg"/>
        </label><br/>
        <input type="submit" value="Сохранить"/>
    </form>
</div>

<script type="text/javascript" src="../../web/assets/2db80a3f/jquery.min.js"></script>
<script type="text/javascript" src="../../web/js/site.js"></script>