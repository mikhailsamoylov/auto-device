<?php

/* @var $this yii\web\View
 * @var $albums array
 */

$this->title = 'Альбомы';
?>
<div class="buttons">
    <input type="button" class="pull-right func-button" value="Добавить альбом" onclick="location.href='/web/?r=site/addalbum';"/>
    <input type="button" class="pull-right func-button" value="Добавить фотографию" onclick="location.href='/web/?r=site/addphoto';"/>
</div>
<br/>
<div class="site-index" id="albums">
    <?php foreach ($albums as $id => $info) { ?>
        <div class="albums" id="album_<?= $id ?>">
            <div class="image">
                <img class="album" src="/resources/<?= is_null($info['path']) ? 'stub.png' : 'photos/' . $info['path'] ?>"/>
            </div>
            <h4 class="title"><?= $info['title'] ?></h4>
            <h3 class="count"><?= $info['count'] ?></h3>
            <h5 class="date"><?= is_null($info['date']) ? ' ' : $info['date'] ?></h5>
            <span class="glyphicon glyphicon-edit edit-album"></span>
            <span class="glyphicon glyphicon-remove remove-album"></span>
        </div>
    <?php } ?>
</div>

<script type="text/javascript" src="../../web/assets/2db80a3f/jquery.min.js"></script>
<script type="text/javascript" src="../../web/js/site.js"></script>