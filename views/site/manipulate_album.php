<?php

/* @var $this yii\web\View
 * @var $albumInfo array
 */

$this->title = 'Добавление альбома';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <form id="album" action="/web/?r=site/savealbum" method="post">
        <input name="album_id" value="<?= isset($albumId) ? $albumId : '' ?>" hidden/>
        <label>Название альбома<br/>
            <input type="text" id="album_title" name="title" value="<?= isset($albumInfo['title']) ? $albumInfo['title'] : '' ?>">
        </label><br/>
        <label>Описание альбома<br/>
            <textarea id="description" name="description"><?= isset($albumInfo['description']) ? $albumInfo['description'] : '' ?></textarea>
        </label><br/>
        <label>Имя фотографа<br/>
            <input type="text" id="name" name="name" value="<?= isset($albumInfo['name']) ? $albumInfo['name'] : '' ?>"/>
        </label><br/>
        <label>Адрес электронной почты<br/>
            <input type="text" id="email" name="email" value="<?= isset($albumInfo['email']) ? $albumInfo['email'] : '' ?>"/>
        </label><br/>
        <label>Контактный телефон<br/>
            <input type="text" id="phone" name="phone" placeholder="+7 (xxx) xx-xx-xx" value="<?= isset($albumInfo['phone']) ? $albumInfo['phone'] : '' ?>">
        </label><br/>
        <input type="submit" value="Сохранить"/>
    </form>
</div>

<script type="text/javascript" src="../../web/assets/2db80a3f/jquery.min.js"></script>
<script type="text/javascript" src="../../web/js/site.js"></script>