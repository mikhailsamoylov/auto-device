<?php

/* @var $this yii\web\View
 * @var $photos array
 * @var $album_id integer
 */

$this->title = 'Фотографии';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="album_<?= $album_id ?>">
    <div class="buttons">
        <input type="button" id="add-photo" class="pull-right func-button" value="Добавить фотографию"/>
    </div>
    <br/>
    <div class="site-index" id="photos">
        <?php foreach ($photos as $info) { ?>
            <div class="photos" id="photo_<?= $info['id'] ?>">
                <div class="image">
                    <a href="/resources/photos/<?= $info['path'] ?>">
                        <img class="photo" src="/resources/photos/<?= $info['path_small'] ?>"/></a>
                </div>
                <span class="glyphicon glyphicon-remove remove-photo"></span>
            </div>
        <?php } ?>
    </div>
</div>

<script type="text/javascript" src="../../web/assets/2db80a3f/jquery.min.js"></script>
<script type="text/javascript" src="../../web/js/site.js"></script>