var validateAlbum = function () {
    var title_length = $('#album_title').val().length;
    if (title_length == 0) {
        alert('Название альбома является обязательным полем для заполнения');
        return false;
    }
    if (title_length > 50) {
        alert('Название альбома должно быть не длиннее 50 символов');
        return false;
    }
    var description_length = $('#description').val().length;
    if (description_length == 0) {
        alert('Описание альбома является обязательным полем для заполнения');
        return false;
    }
    if (description_length > 200) {
        alert('Описание альбома должно быть не длиннее 200 символов');
        return false;
    }
    var name_length = $('#name').val().length;
    if (name_length == 0) {
        alert('Имя автора является обязательным полем для заполнения');
        return false;
    }
    if (name_length > 50) {
        alert('Имя фотографа должно быть не длиннее 50 символов');
        return false;
    }
    var email = $('#email').val();
    if ((email.length != 0) && !/^.+@.+\..+$/.test(email)) {
        alert('Невалидный адрес электронной почты');
        return false;
    }
    var phone = $('#phone').val();
    if ((phone.length != 0) && !/^\+7 \(\d{3}\) \d{3}-\d{2}-\d{2}$/.test(phone)) {
        alert('Невалидный номер телефона');
        return false;
    }
    return true;
};

$('#album').submit(function (event) {
    if (!validateAlbum()) {
        event.preventDefault();
    }
});

$('.remove-album').click(function () {
    if (confirm('Дейсвтительно удалить альбом?')) {
        var album_id = $(this).parent().attr('id');
        $.ajax(
            {
                url: '/web/?r=site/deletealbum&album_id=' + album_id,
                success: function () {
                    $('#' + album_id).remove();
                },
                error: function () {
                    alert('Не удалось удалить альбом');
                }
            });
    }
});

$('.edit-album').click(function () {
    window.location.href = '/web/?r=site/editalbum&album_id=' + $(this).parent().attr('id');
});

$('.album').click(function () {
    window.location.href = '/web/?r=site/viewalbum&album_id=' + $(this).parent().parent().attr('id');
});

$('#add-photo').click(function () {
    window.location.href = '/web/?r=site/addphoto&album_id=' + $(this).parent().parent().attr('id');
});

var validatePhoto = function() {
    var title_length = $('#photo_title').val().length;
    if (title_length == 0) {
        alert('Название фотографии является обязательным полем для заполнения');
        return false;
    }
    if (title_length > 50) {
        alert('Название фотографии должно быть не длиннее 50 символов');
        return false;
    }
    if ($('#address').val().length > 200) {
        alert('Адрес должен быть не длиннее 200 символов');
        return false;
    }
    var file = $('#file');
    if (file.val().length == 0) {
        alert('Выберите файл');
        return false;
    }
    if (file[0].files[0].size > 20 * 1024 * 1024) { // 20mb
        alert('Файл не должен быть больше 20 Мб');
        return false;
    }
    return true;
};

$('#photo').submit(function (event) {
    if (!validatePhoto()) {
        event.preventDefault();
    }
});

$('.remove-photo').click(function () {
    if (confirm('Дейсвтительно удалить фотографию?')) {
        var photo_id = $(this).parent().attr('id');
        $.ajax(
            {
                url: '/web/?r=site/deletephoto&photo_id=' + photo_id,
                success: function () {
                    $('#' + photo_id).remove();
                },
                error: function () {
                    alert('Не удалось удалить фотографию');
                }
            });
    }
});
